import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import * as fb from 'firebase'

Vue.use(Vuetify)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created(){    
    fb.initializeApp({
      apiKey: 'AIzaSyDfmdsovyESSdooMuRH20JjVHHdusPzZxk',
      authDomain: 'newagent-c5cdf.firebaseapp.com',
      databaseURL: 'https://newagent-c5cdf.firebaseio.com',
      projectId: 'newagent-c5cdf',
      storageBucket: 'newagent-c5cdf.appspot.com',
      messagingSenderId: '388110914051'
    });
  }
})
